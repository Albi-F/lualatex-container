FROM alpine:3.21

RUN apk add --no-cache \
zsh \
git \
git-lfs \
gnupg \
openssh \
texlive \
texmf-dist-latexextra \
texmf-dist-fontsextra \
texmf-dist-pictures \
texmf-dist-lang \
texmf-dist-most \
texlive-luatex

LABEL \
    org.opencontainers.image.title="LuaLaTeX-Alpine" \
    org.opencontainers.image.description="${BUILD_DESCRIPTION}" \
    org.opencontainers.image.authors="Alberto Fabbri <alberto_fabbri.git@yahoo.com>" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.url="https://gitlab.com/Albi-F/lualatex-container/" \
    org.opencontainers.image.source="https://gitlab.com/Albi-F/lualatex-container/-/blob/main/Dockerfile/" \
    org.opencontainers.image.documentation="https://gitlab.com/Albi-F/lualatex-container/-/blob/main/README.md" \
    org.opencontainers.image.created=${BUILD_DATE} \
    org.opencontainers.image.revision=${BUILD_REF} \
    org.opencontainers.image.version=${BUILD_VERSION}
