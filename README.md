# LuaLaTeX Container

This repository is used to build a LuaLaTeX container based on [Linux Alpine](https://www.alpinelinux.org/) that can be used with [VS code](https://code.visualstudio.com/) and the [LaTeX Workshop](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop) extension to edit [LuaLaTeX](https://www.luatex.org/) files and compile them into PDFs.

To download the latest version of this container execute the following command:
```console
docker pull registry.gitlab.com/albi-f/lualatex-container:latest
```
